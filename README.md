# R-CODE Samples for ERS-7

Script samples derived from *Developer Primer For Legacy AIBO* presented at AIBO Orlando 2023.

## About

**R-CODE** is a BASIC-like scripting language which are run in an interpreter software designed for Sony entertainment robots built upon the OPEN-R framework. 

Scripts containing R-CODE are stored in `*.R` script files (Not to be confused with R, the statistical language).

## Contents

### Slides

Contains a copy of the slides presented in PDF and OpenDocument formats.

[The original presentation](https://docs.google.com/presentation/d/11E7lJunSRp-3uXguWcTSK2J8idYsSh-9yrYzmK4hbeU/view) is hosted in Google Slides.

### Samples

Contains all sample scripts presented.

* `Meeper.R` - Sample Commands
* `ChubbChubb.R` - Voice Recognition Demo

Other demonstrations are also included.

* `Base/Greeting.R` - Stock Greeting Program
* `Base/Maze.R` - Stock Maze Program
* `Base/Test.R` - Early version of ChubbChubb
* `Base/RCodePlus.R` - The *Mini Obey Cat* minimal personality included in the R-CODE redistributable, which runs when the debugger is not active

## System State Diagram

```mermaid
flowchart TD
    A[System Start] --> B[Battery Locked]
    B --> C[APERIOS Start]
    C --> D["Level 1 Check (Battery, Fan, RTC)"]
    D -->|Low Battery OR Bad| E[Level 1 Failure]
    D --> F["Level 2 Check (PMS, Other)"]
    F -->|Incompatible OR Error| G[Level 2 Failure]
    F -->|No PMS Present| H[Fallback AIBO-ware in firmware]
    F --> I[AIBO-ware Check]
    H --> I
    I --> J(AIBO-ware Initialize)
    J --> K{AIBO-ware Operation}
    K --> K
    K -->|User or Software Initiated Suspend| L(AIBO-ware Exit)
    L --> M[APERIOS Stop]
    M --> N[Battery Unlocked]
    E --> N
    G --> N
    N --> O[System Suspend]
    O -->|Power Button| A
```

---

“AIBO”, “MEMORY STICK”, as well as related names, marks, emblems, and imagery, are copyright, trademarks, or registered trademarks of Sony.
